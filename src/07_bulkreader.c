/***********************************************************************************************************************\
 * Program Name		:	07_bulkreader.c										*
 * Author		:	V. Radhakrishnan ( rk@atr-labs.com )							*
 * License		:	GPL Ver 2.0										*
 * Copyright		:	Cypress Semiconductors Inc. / ATR-LABS							*
 * Date written		:	March 22, 2012										*
 * Modification Notes	:												*
 * 															*
 * This program is a CLI program that does a bulk tranfer using a USB host-to-host cable				*
 * The cable has two type 'A' connectors and when both ends are connected to the same PC, is enumerated twice		*
 * using the same vendor and device ID, in this case VID=0x067b and PID=0x0000						*
\***********************************************************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include "../include/cyusb.h"

/********** Cut and paste the following & modify as required  **********/
static const char * program_name;
static const char *const short_options = "hvt:";
static const struct option long_options[] = {
		{ "help",	0,	NULL,	'h'	},
		{ "version",	0,	NULL,	'v'	},
		{ "timeout",    1,      NULL,   't',	},
		{ NULL,		0,	NULL,	 0	}
};

static int next_option;

static void print_usage(FILE *stream, int exit_code)
{
	fprintf(stream, "Usage: %s options\n", program_name);
	fprintf(stream, 
		"  -h  --help           Display this usage information.\n"
		"  -v  --version        Print version.\n"
		"  -t  --timeout	timeout in seconds, 0 for indefinite wait.\n");

	exit(exit_code);
}
/***********************************************************************/

static FILE *fp = stdout;
static int timeout_provided;
static int timeout = 0;

static void validate_inputs(void)
{
	if ( (timeout_provided) && (timeout < 0) ) {
	   fprintf(stderr,"Must provide a positive value for timeout in seconds\n");
	   print_usage(stdout, 1);
	}   
}

int main(int argc, char **argv)
{
	int r;
	char user_input = 'n';
	cyusb_handle *h1 = NULL;
	int transferred = 0;
	int PacketLength = 768*576*2;
	unsigned char* buf;
	unsigned char* cmdBuf;
	int control_result;
	
	cmdBuf = (unsigned char*)malloc(1); //data to store command result
	
	buf = (unsigned char*)malloc(PacketLength);

	r = cyusb_open();
	h1 = cyusb_gethandle(0);
	r = cyusb_kernel_driver_active(h1, 0);
	
	if ( r != 0 ) 
	{
	   printf("kernel driver active. Exitting\n");
	   cyusb_close();
	   return 0;
	}
	
	r = cyusb_claim_interface(h1, 0);
	if ( r != 0 ) {
	   printf("Error in claiming interface\n");
	   cyusb_close();
	   return 0;
	}
	else printf("Successfully claimed interface\n");


	//memset(buf, '\0', PacketLength);
	
	control_result  = cyusb_control_transfer(h1, 0x40, 0xD1, 0, 0x00, cmdBuf, 0, 0);  //stop
	
	if(control_result == 0)
	{
		printf("Successfully send vendor command!\n");
	} 
	else
	{
		printf("Error in executing vendor command!\n");
	}
	
	control_result = cyusb_control_transfer(h1, 0x40, 0xD0, 0, 0, cmdBuf, 0, 0);  //start
	
	if(control_result == 0)
	{
		printf("VENDOR COMMAND SUCCEED!\n");
	}
	else
	{
		printf("ERROR VENDOR COMMAND!\n");
	}
	
	while (1) 
	{
		r = cyusb_bulk_transfer(h1, 0x82, buf, PacketLength, &transferred, 5000);
		
		if ( r == 0 ) 
		{
		   printf("%s", buf);
		   //memset(buf,'\0',64);
		   //memset(buf, '\0', PacketLength);
		   continue;
		}
		
		else 
		{
		    cyusb_error(r);
		    cyusb_close();
		    return r;
		}
		printf("Transferred Bytes:%d\n", transferred);
	} 
	
	cyusb_close();
	return 0;
}
