CyUSB Linux NIT
===
Camera API driver for [New Imaging Technologies](http://www.new-imaging-technologies.com/) cameras.
See [www.cypress.com](https://www.cypress.com/user/login?destination=file/139281) for the original project.

Dependencies
---
```bash
sudo apt-get install build-essential libusb-1.0-0-dev
```

You also need `qt4` to build the `cyusb_linux` application.
See [README](README) for more details.

How to install
---
```bash
git clone https://gitlab.com/InstitutMaupertuis/cyusb_linux_nit.git
cd cyusb_linux_nit
sudo ./install.sh
```

How to launch
---
```bash
cyusb_linux
```
